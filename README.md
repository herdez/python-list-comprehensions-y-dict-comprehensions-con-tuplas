## List comprehensions y dict comprehensions con tuplas

Para este ejercicio es importante documentarse acerca de `dict comprehensions` en Python.

Define la función `dict_list_comprehension` que recibe una lista con tuplas como argumento. Es importante desarrollar tu código usando `unittest` para verificar que tu programa se comporta y produce el resultado deseado.

Restricción: Se usará únicamente list comprehensions y dict comprehensions, siguiendo las buenas prácticas al estilo pythonista.

```python
"""dict_list_comprehension function"""

...

```
Un ejemplo de argumento recibido sería el siguiente:

```python
[
  (1,'alex',2,'beatriz',3,'carla')  
]
```

Y se retornaría una lista con un diccionario `dict`:

```python
[
  {1: 'alex', 2: 'beatriz', 3: 'carla'} 
]
```
```
# unittest - TDD

$ python test_dict_list_comprehension.py
```

